package com.zuitt.example;

import java.util.Scanner;

public class Activity_1 {
    public static void main(String[] args) {
        System.out.println("Input an integer whose factorial will be computed");

        try {
            Scanner in = new Scanner(System.in);
            int num = in.nextInt();

            int answer = 1;
            int counter = 1;

            if (num == 0 || num == 1) {
                answer = 1;
            }

            while (counter <= num) {
                answer *= counter;
                counter++;
            }

            System.out.println("The factorial of " + num + " is " + answer);

        }
        catch (Exception e) {
            System.out.println("Invalid Input");
            e.printStackTrace();
        }
    }
}
